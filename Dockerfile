# Builder
FROM golang:latest as builder

ARG CGO_ENABLED=0
RUN go get -v github.com/nanopack/shaman 

# Runner
FROM alpine:latest

COPY --from=builder /go/bin/shaman /usr/local/bin/shaman 

RUN mkdir /config 
COPY config.json /config/
COPY startup.sh .
RUN chmod +x startup.sh

ENTRYPOINT ["./startup.sh"]
