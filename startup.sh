#!/bin/sh

echo "Starting shaman dns..."

sleep 2

shaman $(if [ ! "$SHAMAN_API_TOKEN" = "" ]; then echo " --token $SHAMAN_API_TOKEN "; fi) --config-file /config/config.json
